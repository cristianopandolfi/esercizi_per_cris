import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Scanner;
import java.lang.Math;


public class firstPathExercises {

	public static void main(String[] args) {
		
		es3_1();
		
		

	}
	
	//CORREZIONE: i metodi meglio private tanto li usi solo in questa classe
	private static void es1() {
		
//		1. Si scriva un programma che dati tre numeri interi visualizza la media dei tre numeri.

			int a = 20;
			int b = 50;
			int c = 30;

			System.out.println((a + b + c) / 3);

		
		
		
	}
	
	public static void es2() {
		
//		2. Si scriva un programma che date due stringhe le visualizzi ciascuna stringa su una riga distinta, 
//		indicandone la lunghezza. 
//		In seguito si crei una nuova stringa concatenando le due stringhe iniziali ma separandole da uno spazio. 
//		Infine, visualizzare su un terza riga la terza stringa e la sua lunghezza.

			String nome = "Cristiano";
			String cognome = "Pandolfi";
			String fullName = (nome + " " + cognome);

			System.out.println((nome + ", " + "la lunghezza del mio nome �: " + nome.length()));
			System.out.println((cognome + ", " + "la lunghezza del mio cognome �: " + cognome.length()));
			System.out.println((fullName + ", " + "la lunghezza del mio fullName �: " + fullName.length()));

		
		
		
		
	}
	
	private static void es3() {
		// 3. Si scriva un programma che utilizzi la classe Scanner per ricevere in input un numero positivo 

		Scanner sc = new Scanner(System.in);
		int number;

		do {
		  System.out.println("Inserisci un numero positivo");

		  //Continui a chiedere l'input finchè non viene inserito un numero 
		  while (!sc.hasNextInt()) {
			System.out.println("Il carattere inserito non è un numero");
			sc.next(); // questo è importante per andare avanti e richiedere l'input
		  }
		  
		  //Quando viene inserito un numero intero il while sopra smette di essere eseguito (la condizione ritorna false)
		  number = sc.nextInt();

		} while (number <= 0); //Controllo number e finchè il numero inserito non è positivo rimango nel loop

		System.out.println("Hai inserito un carattere valido " + number);

	}

	private static void es3_1(){
		// 3.1 inserimento di un carattere contenente soltanto numeri da 0 a 9.
				
		Scanner scanner = new Scanner(System.in);
				
		System.out.println("inserisci una sola parola contenente soltanto numeri da 0 a 9.");
		
		String input = scanner.next();
		
		System.out.println("Hai inserito: " + input);

		//Macthes metodo per controllare che il carattere inserito sia contenuto nella regular expression (regex) 0-9
		if(input.matches("^[0-9]")) {
			System.out.println("Carattere Valido");

		} else {
			System.out.println("Carattere non consentito");
		}

		scanner.close();
	}
	
	public static void es4() {
		
		// 4. Si scriva un programma che legga una riga di testo (Scanner) 
		// e poi la visualizzi sostituendo la prima occorrenza della parola "odio" con "amore". 
		// Esempio: Io ti odio -> Io ti amo		

				Scanner sostituzione = new Scanner(System.in);

				System.out.println("Inserisci in una frase la parola 'odio'.");

				String frase = sostituzione.nextLine();
				System.out.println("Frase:" + " " + frase + ".");

				frase = frase.replaceAll("odio", "amo");
				System.out.println("Frase sostituita:" + " " + frase + ".");
		
	}
	
	public static void es5() {
		
		// 5. Si scriva un programma che legga una frase in input e la visualizzi dopo aver spostato la prima parola al fondo della frase.
//		Es: Java � un linguaggio -> � un linguaggio Java

			Scanner cut = new Scanner(System.in);

			System.out.println("inserisci in una frase la parola che verr� spostata: ");

			String phrase = cut.nextLine();
			System.out.println("Frase:" + " " + phrase + ".");

			phrase = phrase.replace("java � un linguaggio", "� un linguaggio java");
			System.out.println("parola spostata:" + " " + phrase + ".");

		
		
		
	}
	
	public static void es6() {
		
		// 6. Si scriva un programma che verifichi la validit� di un numero intero inserito in input, 
//		un numero � valido se compreso tra 0 e 100. Si visualizzi la stringa "Valido" oppure "Invalido".

			Scanner number = new Scanner(System.in);

			System.out.println("inserisci numero: ");

			int numbers = number.nextInt();

			System.out.println("Numero inserito:" + " " + numbers);

			if (numbers <= 100) { // avrei potuto usare anche l'espressione "OR" ma avrei allungato il codice :
									// (numbers < 100 || numbers == 100)

				System.out.println("Valido");

			} else {
				System.out.println("Invalido");
			}

	}

	public static void es7() {
		
		// 7. Si scriva un programma che controlli se un numero � divisibile per un altro. 
		// Entrambi i numeri devono essere letti da tastiera. 
		// N.B. un numero � divisibile per un altro se il resto della loro divisione � pari a 0.

				Scanner divisione = new Scanner(System.in);

				System.out.println("Primo numero:");
				float number1 = divisione.nextFloat();

				System.out.println("Secondo numero:");
				float number2 = divisione.nextFloat();

				System.out.println("I numeri da dividere sono:" + " " + number1 + " e " + number2);

				float risultato = number1 / number2;
				System.out.println("Il risultato della divisione � " + risultato);

				if (risultato != 0.0) {
					System.out.println("Risutato valido");
				} else {
					System.out.println("Risultato non valido");
				}
		
	}

	public static void es8() {
		
		// 8. Si scriva un programma che legga 3 numeri in input e li visualizzi in ordine crescente.

				Scanner ordine = new Scanner(System.in);

				System.out.println("Inserire il primo numero");

				int primo = ordine.nextInt();
				System.out.println("Primo numero: " + primo);

				System.out.println("Inserire il secondo numero");

				int secondo = ordine.nextInt();
				System.out.println("Secondo numero: " + secondo);

				System.out.println("Inserire il terzo numero");

				int terzo = ordine.nextInt();
				System.out.println("Terzo numero: " + terzo);

				if (primo < secondo) {
					if (primo < terzo) {
						if (secondo < terzo)
							System.out.println(primo + " " + secondo + " " + terzo);
						else
							System.out.println(primo + " " + terzo + " " + secondo);
					} else
						System.out.println(terzo + " " + primo + " " + secondo);
				} else {
					if (secondo < terzo) {
						if (primo < terzo)
							System.out.println(secondo + " " + primo + " " + terzo);
						else
							System.out.println(secondo + " " + terzo + " " + primo);
					} else
						System.out.println(terzo + " " + secondo + " " + primo);
				}
				
	}

}
